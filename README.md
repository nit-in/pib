# pib

Download articles from [Press Information Bureau, India](https://www.pib.gov.in).
This might be helpful for candidates preparing for different govt examinations.

## How to use:

```shell

#Clone the repo with:
git clone https://github.com/nit-in/pib
#cd to the cloned repo
cd pib
#installing required packages
pip install -r requirements.txt
#when these steps are done,you are ready to run the spider and download the articles.

#source the env file
source .env
#run the spider
pib start_date end_date #(date format: yyyy-mm-dd)
#example => to download the articles for the month of june,2021; use
pib 2021-06-01 2021-06-30
```

Any suggestions and improvements are welcome.
